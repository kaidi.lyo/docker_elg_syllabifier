# Eesti keele silbitaja konteiner (versioon 2022.08.24)

[Eesti keele silbitajat](https://cl.ut.ee/korpused/silbikorpus/silbita.xfscript) sisaldav tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Mida sisaldab <a name="Mida_sisaldab"></a>

* [Lõplik muundur eestikeelsete sõnede silbitamiseks](https://cl.ut.ee/korpused/silbikorpus/silbita.xfscript) 
* [Filosofti eesti keele morfoloogiline analüsaator](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/LOEMIND.md)
* [Skriptid muunduri ja analüsaatori ühendamiseks](https://gitlab.com/tilluteenused/docker_elg_syllabifier)
* Konteineri ja liidesega seotud lähtekood  

## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).
* Kui sooviks on lähtekoodi ise kompileerida või konteinerit kokku panna, siis peab olema paigaldatud versioonihaldustarkvara; juhised on [git'i veebilehel](https://git-scm.com/).


## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i/Windows'i/Mac'i käsurida:

```commandline
docker pull tilluteenused/est_elg_syllabifier:2022.08.24
```

Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine Linux'is

### 1. [Konteineri ja liidesega seotud lähtekoodi](https://gitlab.com/tilluteenused/docker_elg_syllabifier) allalaadimine

```cmdline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone --branch 2022_08_24 --depth 1 https://gitlab.com/tilluteenused/docker_elg_syllabifier.git gitlab_docker_elg_syllabifier
```

Repositoorium sisaldab silbitamise muundurit ja skripte ning kompileeritud [Filosofti morfoloogilist analüsaatorit](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/LOEMIND.md) ja andmefaile:

* **_vmeta_**  - morfoloogilise analüüsi programm.
* **_et.dct_** - programmi poolt kasutatav leksikon.

Kui soovite ise programmi (**_vmeta_**) kompileerida või leksikoni (**_et.dct_**) täiendada/muuta ja uuesti kokku panna, 
vaadake sellekohast [juhendit](https://github.com/Filosoft/vabamorf/blob/master/doc/programmid_ja_sonastikud.md).

### 2. Konteineri kokkupanemine

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_syllabifier
docker build -t tilluteenused/est_elg_syllabifier:2022.08.24 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

Konteineri käivitamine kasutades Linux'i/Windows'i/Mac'i käsurida:

```commandline
docker run -p 7000:7000 tilluteenused/est_elg_syllabifier:2022.08.24
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati.

## Päringu json-kuju

```json
{
  "type":"text",
  "content": string, /* Silbitatavad sõned tühikuga eraldatult, punktuatsioon sõnest lahus */
}
```

## Vastuse json-kuju

```json
{
  "response":
  {
    "type":"texts",
    "texts":
    [ /* silbitatud sõnede massiiv */
      {
          "content":str, /* silbitatav sõne */
          "features":
          {
              "syllables":str,  /* silbitatud sõne; osasõna piiriks '_', silbipiiriks '.' */
              "warning":str     /* ainult siis, kui ei saanud silbitada */ 
          }
      }
    ]
  }
}
```

## Kasutusnäited

Kasutusnäide Linux'i/Windows'i/Mac'i käsurealt:

```cmdline
curl --silent --request POST --header "Content-Type: application/json" --data "{\"type\":\"text\", \"content\":\"vanaisa . 124 bla-blabla\"}" localhost:7000/process
```

```json
{"response":{"type":"texts","texts":[{"content":"vanaisa","features":{"syllables":"va.na_i.sa"}},{"content":".","features":{"syllables":"","warning":"pole silbitatud"}},{"content":"124","features":{"syllables":"","warning":"pole silbitatud"}},{"content":"bla-blabla","features":{"syllables":"bla-blab.la"}}]}}
```

Kasutusnäide Linux'i käsurealt:

```cmdline
curl --silent --request POST --header "Content-Type: application/json" --data '{"type":"text", "content":"vanaisa . 124 bla-blabla"}' localhost:7000/process | jq
```

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "content": "vanaisa",
        "features": {
          "syllables": "va.na_i.sa"
        }
      },
      {
        "content": ".",
        "features": {
          "syllables": "",
          "warning": "pole silbitatud"
        }
      },
      {
        "content": "124",
        "features": {
          "syllables": "",
          "warning": "pole silbitatud"
        }
      },
      {
        "content": "bla-blabla",
        "features": {
          "syllables": "bla-blab.la"
        }
      }
    ]
  }
}
```

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.
 



# Container with a syllabifier for Estonian (version 2022.08.24)
[Syllabifier for Estonian](https://cl.ut.ee/korpused/silbikorpus/silbita.xfscript) container (docker) with
interface compliant with [ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Contains  <a name="Contains"></a>

* [Finite state transducer for syllabifying Estonian](https://cl.ut.ee/korpused/silbikorpus/silbita.xfscript)
* [Filosoft morphological analyzer](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/)
* [Scripts for combining syllabifier and analyzer](https://gitlab.com/tilluteenused/docker_elg_syllabifier) 
* Container and interface code

## Preliminaries

* You should have software for making / using the container installed; see instructions on the [docker web site](https://docs.docker.com/).
* In case you want to compile the code or build the container yourself, you should have version control software installed; see instructions on the [git web site](https://git-scm.com/).

## Downloading image from Docker Hub

You may dowload a ready-made container from Docker Hub, using the Linux command line (Windows / Mac command lines are similar):

```commandline
docker pull tilluteenused/est_elg_syllabifier:2022.08.24
```

Next, continue to the section [Starting the container](#Starting_the_container).

## Making your own container


### 1. Downloading [container and interface code](https://gitlab.com/tilluteenused/docker_elg_syllabifier) 

```cmdline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone --branch 2022_08_24 --depth 1 https://gitlab.com/tilluteenused/docker_elg_syllabifier.git gitlab_docker_elg_syllabifier
```

The repo contains the transducer and scripts for syllabifying and  a compiled [morphological analyzer](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/) by Filosoft:

* **_vmeta_** - programme for morphological analysis
* **_et.dct_** - lexicon used by the analyzer 

In case you want to compile the program (**_vmeta_**) or change and re-assemble the lexicon (**_et.dct_**), follow the [instructions](https://github.com/Filosoft/vabamorf/blob/master/doc/make_programs_and_lexicons.md).

### 2. Building the container

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_syllabifier
docker build -t tilluteenused/est_elg_syllabifier:2022.08.24 .
```

## Starting the container <a name="Starting_the_container"></a>

```commandline
docker run -p 7000:7000 tilluteenused/est_elg_syllabifier:2022.08.24
```

One need not be in a specific directory to start the container.

Ctrl+C in a terminal window with a running container in it will terminate the container.

## Query json

```json
{
  "type":"text",
  "content": string, /* Words delimited by space; punctuation separate from words */
}
```

## Vastuse json-kuju

```json
{
  "response":
  {
    "type":"texts",
    "texts":
    [ /* array of syllabified tokens */
      {
          "content":str, /* input token */
          "features":
          {
              "syllables":str,  /* syllabified token; compound part border is '_', syllable border is '.' */
              "warning":str     /* only if syllabifying was impossible */ 
          }
      }
    ]
  }
}
```

## Usage example

```cmdline
curl --silent --request POST --header "Content-Type: application/json" --data '{"type":"text", "content":"vanaisa . 124 bla-blabla"}' localhost:7000/process | jq
```

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "content": "vanaisa",
        "features": {
          "syllables": "va.na_i.sa"
        }
      },
      {
        "content": ".",
        "features": {
          "syllables": "",
          "warning": "pole silbitatud"
        }
      },
      {
        "content": "124",
        "features": {
          "syllables": "",
          "warning": "pole silbitatud"
        }
      },
      {
        "content": "bla-blabla",
        "features": {
          "syllables": "bla-blab.la"
        }
      }
    ]
  }
}
```

## Sponsors

The container development was sponsored by EU CEF project [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry)


## Authors

Authors of the container: Tarmo Vaino, Heiki-Jaan Kaalep

Authors of the contents of the container: see references at section [Contains](#Contains).

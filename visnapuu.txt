Üle kodumäe kumeda kupli 
suveöö sumestav valu . 
Toomingad valvavad talu , 
üle ussaia vaatavad kopli . 

Aian õunapuud punasest lõngast 
ketravad magusaid palle . 
Vaatavad armuga põlle 
nurmed visaten rohelist rõngast . 

Oja leppade jalgu nüüd peseb 
kivile kivi päält asten . 
Odrapää mahedan kasten 
kaera pehmete sarviga puseb . 

Maja vajunud rõõmsasse unne . 
Muru pääl magavad rohun 
isegi haldijad rahun . 
Silmik seinakell üksi teeb tunde . 


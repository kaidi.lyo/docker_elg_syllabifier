#!/usr/bin/python3

import sys
import json
import requests

'''
Kasutusnäide
* installi docker
* Laadi alla ja käivita silbitajat sisaldav dockeri konteiner
  > docker pull tilluteenused/est_elg_syllabifier:2022.08.24
  > docker run -p 7000:7000 tilluteenused/est_elg_syllabifier:2022.08.24
 * installi python
* tektita pythoni jooksutamiseks vajalik virtuaalkeskkond
  > ./create_venv.sh
* tekita fail silbitatava tekstiga
  NB! ei silbita sõnesid, kus punktuatsioon on sõnega kokkukleepunud!
  Näiteks selline fail:
  > cat test.txt
  naistrummar
  peeti kinni.
* Lase pythoni näidisprogrammil oma tekst silbitada
  > venv/bin/python3 test.py test.txt            
  naistrummar nais_trum.mar
  peeti pee.ti
  kinni.
'''


def silbita(sisend:str) -> str:
    """Silbitamine

    Args:
        text (str): Silbitatavad sõned json-stringina

    Returns:
        str: Silbitatud värk
    """
    return requests.post('http://localhost:7000/process', json=json.loads(sisend)).text

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('FILE', help='sisendfailid')                       
    args = argparser.parse_args()

    with open(args.FILE, 'r') as file:
        raw_data = file.read()
        data = json.dumps(raw_data.replace('\n', ' ').replace('\r', ' ').replace('\t', ' ').strip())
        sisend = f"{{\"type\":\"text\", \"content\":{data}}}"
        valjund = json.loads(silbita(sisend))
        for tulemus in valjund["response"]["texts"]:
                print(tulemus["content"], tulemus["features"]["syllables"])

    
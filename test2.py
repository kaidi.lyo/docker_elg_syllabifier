#!/usr/bin/python3

import sys
import json
import requests

'''
Kasutusnäide
* installi docker
* Laadi alla ja käivita silbitajat sisaldav dockeri konteiner
  > docker pull tilluteenused/est_elg_syllabifier:2022.08.24
  > docker run -p 7000:7000 tilluteenused/est_elg_syllabifier:2022.08.24
 * installi python
* installi Python'i requests pakett
  * Windows'is pip'iga (täpne käsurida sõltub sellest, millise Python'i olete installinud)
  * Ubuntu's apt-get'iga
* tekita fail silbitatava tekstiga
  NB! ei silbita sõnesid, kus punktuatsioon on sõnega kokku kleepunud!
* tõmba GITLABist näidisprogramm test2.py
* Lase pythoni näidisprogrammil oma visnapuu luuletus silbitada (Windowsi käsurida veidi erinev)
  > ./test2.py visnapuu.txt            
  ü.le ko.du_mäe ku.me.da kup.li 
  su.ve_öö su.mes.tav va.lu . 
  too.min.gad val.va.vad ta.lu , 
  ü.le uss_ai.a vaa.ta.vad kop.li . 

  ai.an õu.na_puud pu.na.sest lõn.gast 
  ket.ra.vad ma.gu.said pal.le . 
  vaa.ta.vad ar.mu.ga põl.le 
  nur.med vi.sa.ten ro.he.list rõn.gast . 

  o.ja lep.pa.de jal.gu nüüd pe.seb 
  ki.vi.le ki.vi päält as.ten . 
  od.ra.pää ma.he.dan kas.ten 
  kae.ra peh.me.te sarv_i.ga pu.seb . 

  ma.ja va.ju.nud rõõm.sas.se un.ne . 
  mu.ru pääl ma.ga.vad ro.hun 
  i.se.gi hal.di.jad ra.hun . 
  sil.mik sei.na_kell ük.si teeb tun.de . 
'''

def silbita(sisend): 
    """Silbitamine

    Args:
        text (str): json-päring stringina

    Returns:
        str: silbitatud värk
    """
    return requests.post('http://localhost:7000/process', json=json.loads(sisend)).text

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('FILE', help='sisendfailid')                       
    args = argparser.parse_args()

    with open(args.FILE, 'r') as file:
        for line in file: # tsükkel üle luuletuse ridade
          stripped_line = json.dumps(line.replace('\t', ' ').strip())
          if len(stripped_line) == 0:
            sys.stdout.write('\n') # tühi rida läheb väljundisse tühja reana
          valjund = json.loads(silbita(f"{{\"type\":\"text\", \"content\":{stripped_line}}}"))
          for tulemus in valjund["response"]["texts"]: # tsükkel üle jooksva rea silbitatud sõnade
            if len(tulemus["features"]["syllables"]) == 0:
              # mida ei õnnestunud silbitada paneme algsel kujul
              sys.stdout.write(f'{tulemus["content"]} ')
            else:
              # sõna silbitatud kujul väljundisse
              sys.stdout.write(f'{tulemus["features"]["syllables"]} ')
          sys.stdout.write('\n')
